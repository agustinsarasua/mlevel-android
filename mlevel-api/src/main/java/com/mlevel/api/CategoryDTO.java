package com.mlevel.api;

/**
 * Created by agustin on 26/04/16.
 */
public class CategoryDTO {

    public String name;

    public enum Type {
        BUSINESS,
        CHARITY,
        POLITICS,
        FILMS,
        SPORTS,
        MUSIC,
        ART,
        HOBBIES,
        FASHION,
        HEALTH,
        FOOD_AND_DRINK,
        OTHER
    }
}
