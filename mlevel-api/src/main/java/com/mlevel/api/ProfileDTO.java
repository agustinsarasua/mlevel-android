package com.mlevel.api;

/**
 * Created by agustin on 15/03/16.
 */
public class ProfileDTO extends AbstractDTO {

    private String name;

    private String imgUrl;

    private String welcomeMessage;

    private String sex;

    // Discovery Preferences
    private boolean discovery;

    private int minAge;

    private int maxAge;

    //El sexo que esta interesado que le aprezca cuando se trata de desconocidos.
    private String discoverySex;

    public String getDiscoverySex() {
        return discoverySex;
    }

    public void setDiscoverySex(String discoverySex) {
        this.discoverySex = discoverySex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public boolean isDiscovery() {
        return discovery;
    }

    public void setDiscovery(boolean discovery) {
        this.discovery = discovery;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }
}
