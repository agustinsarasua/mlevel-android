package com.mlevel.api.timeline;

import java.util.List;

/**
 * Created by agustin on 16/03/16.
 */
public class EventDTO extends TimelineItemDTO {

    public String facebookId;

    public CoverDTO coverDTO;

    public String eventType;

    public List<ActionDTO> commonActions;

    @Override
    public TimelineItemType getType() {
        return TimelineItemType.EVENT;
    }
}
