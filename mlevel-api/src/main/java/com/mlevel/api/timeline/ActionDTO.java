package com.mlevel.api.timeline;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 17/03/16.
 */
public class ActionDTO extends AbstractDTO {

    public String name;

    public int icon;

    public ActionDTO(){}

    public ActionDTO(String name, int imgUrl){
        this.name = name;
        this.icon = imgUrl;
    }
}
