package com.mlevel.api.timeline;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 23/04/16.
 */
public class CoverDTO extends AbstractDTO{

    public String uuid;

    public String source;

    public double offsetX;

    public double offsetY;

}
