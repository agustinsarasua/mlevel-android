package com.mlevel.api.timeline;

import com.mlevel.api.AbstractDTO;
import com.mlevel.api.location.PlaceDTO;

import java.util.Date;

/**
 * Created by agustin on 02/04/16.
 */
public abstract class TimelineItemDTO extends AbstractDTO {

    public String uuid;

    public String name;

    public String subtitle;

    public String description;

    public PlaceDTO placeDTO;

    public Date startTime;

    public Date endTime;

    public Date creationDate;

    public abstract TimelineItemType getType();

    public enum TimelineItemType {
        EVENT, OPEN_INVITATION
    }

}
