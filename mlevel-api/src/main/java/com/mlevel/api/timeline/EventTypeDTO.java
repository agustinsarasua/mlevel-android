package com.mlevel.api.timeline;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 16/03/16.
 */
public class EventTypeDTO extends AbstractDTO {

    public enum EventType{
        PROMO_AEREA,
        FOOTBALL_MATCH,
        CINEMA_FILM,
        CONCERT
    }

    public String name;

    public String description;

}
