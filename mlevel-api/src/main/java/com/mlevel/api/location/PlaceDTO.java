package com.mlevel.api.location;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 23/04/16.
 */
public class PlaceDTO extends AbstractDTO {

    public String uuid;

    public String facebookId;

    public String name;

    public LocationDTO locationDTO;

}
