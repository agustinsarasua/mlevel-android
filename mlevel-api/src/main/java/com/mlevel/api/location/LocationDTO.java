package com.mlevel.api.location;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 10/03/16.
 */
public class LocationDTO extends AbstractDTO {

    public double latitude;

    public double longitude;

    public String city;

    public String country;

    public String zipCode;

    public String street;

    public LocationDTO(double lat, double lng){
        this.latitude = lat;
        this.longitude = lng;
    }

}
