package com.mlevel.api;

import com.mlevel.api.timeline.CoverDTO;

/**
 * Created by agustin on 24/04/16.
 */
public class UserDTO extends AbstractDTO {

    public String uuid;

    public String name;

    public String lastName;

    public String message;

    public CoverDTO coverDTO;

}
