package com.mlevel.api.authentication.request;

import com.mlevel.api.AbstractDTO;

/**
 * Created by agustin on 06/03/16.
 */
public class PasswordRequestDTO extends AbstractDTO {

    private String password;

    public PasswordRequestDTO() {}

    public PasswordRequestDTO(final String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
