package com.mlevel.pimba.backend;

import com.mlevel.api.UserDTO;
import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.TimelineItemDTO;

import java.util.Date;
import java.util.List;

/**
 * Created by agustin on 02/04/16.
 */
public interface DataSupplier {

    //AuthenticatedUserTokenDTO loginFacebook(OAuth2RequestDTO requestDTO);

    List<TimelineItemDTO> loadTimelineItems(int count, Date lastQuery, Date olderItem);

    List<UserDTO> loadFavouriteUsers();

    EventDTO loadEventById(String uuid);
}
