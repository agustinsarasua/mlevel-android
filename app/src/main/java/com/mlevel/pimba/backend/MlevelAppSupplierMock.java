package com.mlevel.pimba.backend;

import com.mlevel.api.UserDTO;
import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.location.PlaceDTO;
import com.mlevel.api.timeline.ActionDTO;
import com.mlevel.api.timeline.CoverDTO;
import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.EventTypeDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by agustin on 02/04/16.
 */
public class MlevelAppSupplierMock implements DataSupplier {

    List<TimelineItemDTO> randomItems = null;

    {
        randomItems = new ArrayList<>();
        randomItems.add(randomEvent("Promo Viaje a New York", "Aprovecha esta oferta que se agota y vola", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Se estrena el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Barcelo vs Real Madrid", "A las 4 por directv plus", EventTypeDTO.EventType.FOOTBALL_MATCH));
        randomItems.add(randomEvent("Se estrena de nuevo el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Pascualinas del litoral", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Promo Viaje a New York", "Aprovecha esta oferta que se agota y vola", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Se estrena el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Barcelo vs Real Madrid", "A las 4 por directv plus", EventTypeDTO.EventType.FOOTBALL_MATCH));
        randomItems.add(randomEvent("Se estrena de nuevo el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Pascualinas del litoral", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Promo Viaje a New York", "Aprovecha esta oferta que se agota y vola", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Se estrena el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Barcelo vs Real Madrid", "A las 4 por directv plus", EventTypeDTO.EventType.FOOTBALL_MATCH));
        randomItems.add(randomEvent("Se estrena de nuevo el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Pascualinas del litoral", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Promo Viaje a New York", "Aprovecha esta oferta que se agota y vola", EventTypeDTO.EventType.PROMO_AEREA));
        randomItems.add(randomEvent("Se estrena el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Barcelo vs Real Madrid", "A las 4 por directv plus", EventTypeDTO.EventType.FOOTBALL_MATCH));
        randomItems.add(randomEvent("Se estrena de nuevo el lobo de wall street", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.CINEMA_FILM));
        randomItems.add(randomEvent("Pascualinas del litoral", "Sala 5 en movie bal bal bal balbalb a", EventTypeDTO.EventType.PROMO_AEREA));
    }
    /*@Override
    public AuthenticatedUserTokenDTO loginFacebook(OAuth2RequestDTO requestDTO) {
        AuthenticatedUserTokenDTO mock = new AuthenticatedUserTokenDTO();
        mock.setUserId("user-uuid-etc");
        mock.setToken("user-token-etc");
        return mock;
    }*/

    @Override
    public List<TimelineItemDTO> loadTimelineItems(int count, Date lastQuery, Date olderItem) {
        return randomItems.subList(0, count);
    }

    @Override
    public List<UserDTO> loadFavouriteUsers() {
        List<UserDTO> userDTOs = new ArrayList<>();
        userDTOs.add(randomUser("Agustin", "Sarasua"));
        userDTOs.add(randomUser("Mac", "uco"));
        userDTOs.add(randomUser("Sopa", "Boba"));
        userDTOs.add(randomUser("Yissel", "Limpia"));
        userDTOs.add(randomUser("Juan", "Puelbo"));
        userDTOs.add(randomUser("Secretaria", "Marcketing"));
        return userDTOs;
    }

    @Override
    public EventDTO loadEventById(String uuid) {
        for(TimelineItemDTO timelineItemDTO : randomItems){
            if(timelineItemDTO.uuid.equals(uuid)){
                return (EventDTO)timelineItemDTO;
            }
        }
        return null;
    }

    private UserDTO randomUser(String name, String lastName){
        UserDTO userDTO = new UserDTO();
        userDTO.name = name;
        userDTO.lastName = lastName;
        userDTO.coverDTO = new CoverDTO();
        userDTO.message = "Hey there, Im using Pimba!";
        userDTO.coverDTO.source = "http://lorempixel.com/200/200/";
        return userDTO;
    }

    private PlaceDTO randomPlace(){
        PlaceDTO placeDTO = new PlaceDTO();
        placeDTO.name="La Habana";
        placeDTO.locationDTO = new LocationDTO(-34.881812, -56.093316);
        placeDTO.locationDTO.city = "Montevideo";
        return placeDTO;
    }

    private EventDTO randomEvent(String name, String subtitle, EventTypeDTO.EventType type){
        EventDTO itemDTO = new EventDTO();
        itemDTO.name = name;
        itemDTO.subtitle = subtitle;
        itemDTO.placeDTO = randomPlace();
        itemDTO.description = name + " - " + subtitle;
        itemDTO.creationDate = new Date();
        itemDTO.startTime = new Date();
        itemDTO.endTime = new Date();
        itemDTO.uuid = UUID.randomUUID().toString();
        itemDTO.coverDTO = new CoverDTO();
        itemDTO.coverDTO.source = "http://lorempixel.com/400/400/";
        itemDTO.eventType = type.name();
        itemDTO.commonActions = new ArrayList<>();
        itemDTO.commonActions.add(new ActionDTO("Tomar una cerveza", R.drawable.ic_beer));
        itemDTO.commonActions.add(new ActionDTO("Tomar un martini con una aceituna", R.drawable.ic_martini));
        return itemDTO;
    }
}
