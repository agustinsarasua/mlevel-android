package com.mlevel.pimba.backend;

import com.mlevel.api.UserDTO;
import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.rest.RestConnector;

import java.util.Date;
import java.util.List;

/**
 * Created by agustin on 25/03/16.
 */
public class MlevelAppSupplier implements DataSupplier {

    public static final String LOGIN_FB_URL = "/mlevel/service/user/login/facebook";

    public static final String UPDATE_LOCATION_URL = "/mlevel/service/location";

/*    public AuthenticatedUserTokenDTO loginFacebook(OAuth2RequestDTO requestDTO){
        RestConnector restConnector = new RestConnector();
        AuthenticatedUserTokenDTO result = null;
        try {
            result = (AuthenticatedUserTokenDTO) restConnector.doPost(LOGIN_FB_URL, requestDTO , AuthenticatedUserTokenDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }*/

    @Override
    public List<TimelineItemDTO> loadTimelineItems(int count, Date lastQuery, Date olderItem) {
        return null;
    }

    @Override
    public List<UserDTO> loadFavouriteUsers() {
        return null;
    }

    @Override
    public EventDTO loadEventById(String uuid) {
        return null;
    }

}
