package com.mlevel.pimba.backend;

/**
 * Created by agustin on 02/04/16.
 */
public class RestClient {

    public static final boolean USE_MOCKS = true;

    private static DataSupplier mSupplier;

    public static DataSupplier getInstance() {
        if(mSupplier == null) {
            if (USE_MOCKS) {
                mSupplier = new MlevelAppSupplierMock();
            } else {
                mSupplier = new MlevelAppSupplier();
            }
        }
        return mSupplier;
    }



}
