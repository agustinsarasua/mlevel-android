package com.mlevel.pimba;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mlevel.api.CategoryDTO;
import com.mlevel.pimba.adapter.RecyclerPersonalizeAdapter;

public class PersonalizeActivity extends AppCompatActivity {

    private GridLayoutManager lLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalize);

        CategoryDTO.Type[] categories = CategoryDTO.Type.values();

        lLayout = new GridLayoutManager(PersonalizeActivity.this, 4);

        RecyclerView rView = (RecyclerView)findViewById(R.id.category_recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        RecyclerPersonalizeAdapter rcAdapter = new RecyclerPersonalizeAdapter(PersonalizeActivity.this, categories);
        rView.setAdapter(rcAdapter);
    }
}
