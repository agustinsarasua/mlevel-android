package com.mlevel.pimba.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import com.mlevel.api.UserDTO;
import com.mlevel.pimba.R;
import com.mlevel.pimba.adapter.RecyclerFriendAdapter;
import com.mlevel.pimba.adapter.RecyclerTimelineAdapter;
import com.mlevel.pimba.backend.DataSupplier;
import com.mlevel.pimba.backend.RestClient;
import com.mlevel.pimba.util.HidingScrollListener;

import java.util.List;


public class FavouriteUsersFragment extends Fragment {

    private RecyclerView recyclerView;

    private RecyclerFriendAdapter friendAdapter;

    private ProgressBar progressBar;

    private Context mContext;

    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite_users, container, false);

        initRecyclerView(view);

        progressBar = (ProgressBar) view.findViewById(R.id.friends_progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        new LoadFavouriteUsersTask().execute();

        // Inflate the layout for this fragment
        return view;
    }

    private void initRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.friendsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }


    private void hideViews() {
        mToolbar.animate().translationY(-mToolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        mToolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    class LoadFavouriteUsersTask extends AsyncTask<Void, Void, List<UserDTO>> {

        @Override
        protected List<UserDTO> doInBackground(Void... params) {
            try {
                DataSupplier client = RestClient.getInstance();
                return client.loadFavouriteUsers();
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(List<UserDTO> items) {
            friendAdapter = new RecyclerFriendAdapter(items, mContext);
            recyclerView.setAdapter(friendAdapter);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

}
