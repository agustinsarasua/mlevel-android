package com.mlevel.pimba.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.MainActivity;
import com.mlevel.pimba.R;
import com.mlevel.pimba.TimelineItemDetailActivity;
import com.mlevel.pimba.adapter.RecyclerTimelineAdapter;
import com.mlevel.pimba.backend.DataSupplier;
import com.mlevel.pimba.backend.RestClient;
import com.mlevel.pimba.util.EndlessRecyclerViewScrollListener;
import com.mlevel.pimba.util.HidingScrollListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TimelineFragment extends Fragment {

    private RecyclerView recyclerView;

    private RecyclerTimelineAdapter timelineAdapter;

    private ProgressBar progressBar;

    private Context mContext;

    private Toolbar mToolbar;

    final List<TimelineItemDTO> timelineItemDTOs = new ArrayList<>();

    public TimelineFragment() {

    }

    RecyclerTimelineAdapter.OnItemClickListener onItemClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onItemClickListener = new RecyclerTimelineAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, String uuid) {
                Intent transitionIntent = new Intent(getActivity(), TimelineItemDetailActivity.class);
                transitionIntent.putExtra(TimelineItemDetailActivity.EXTRA_PARAM_ID, uuid);
                ImageView placeImage = (ImageView) v.findViewById(R.id.cover);

                //LinearLayout placeNameHolder = (LinearLayout) v.findViewById(R.id.placeNameHolder);

                View navigationBar = getActivity().findViewById(android.R.id.navigationBarBackground);
                View statusBar = getActivity().findViewById(android.R.id.statusBarBackground);

                Pair<View, String> imagePair = Pair.create((View) placeImage, "tImage");
                //Pair<View, String> holderPair = Pair.create((View) placeNameHolder, "tNameHolder");
                Pair<View, String> navPair = Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME);
                Pair<View, String> statusPair = Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME);
                //Pair<View, String> toolbarPair = Pair.create((View)toolbar, "tActionBar");

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), imagePair);
                //ActivityCompat.startActivity(getActivity(), transitionIntent, options.toBundle());
                //getActivity().startActivity(transitionIntent, options.toBundle());
                startActivity(transitionIntent);
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        // Set the mAdapter
        // recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        initRecyclerView(view);

        progressBar = (ProgressBar) view.findViewById(R.id.timeline_progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        new LoadTimelineItemsTask().execute(6, new Date(), new Date());
        return view;
    }



    private void initRecyclerView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                customLoadMoreDataFromApi(page);
            }
        });


//        recyclerView.setOnScrollListener(new HidingScrollListener() {
//            @Override
//            public void onHide() {
//                hideViews();
//            }
//
//            @Override
//            public void onShow() {
//                showViews();
//            }
//        });
    }

    // Append more data into the adapter
    // This method probably sends out a network request and appends new data items to your adapter.
    public void customLoadMoreDataFromApi(int offset) {
        // Send an API request to retrieve appropriate data using the offset value as a parameter.
        // Deserialize API response and then construct new objects to append to the adapter
        // Add the new objects to the data source for the adapter
        DataSupplier client = RestClient.getInstance();
        List<TimelineItemDTO> items = client.loadTimelineItems(offset, new Date(), new Date());
        timelineItemDTOs.addAll(items);
        // For efficiency purposes, notify the adapter of only the elements that got changed
        // curSize will equal to the index of the first element inserted because the list is 0-indexed
        int curSize = timelineAdapter.getItemCount();
        timelineAdapter.notifyItemRangeInserted(curSize, items.size() - 1);

        //new LoadMoreTimelineItemsTask().execute(offset, new Date(), new Date());

    }

//    private void hideViews() {
//        mToolbar.animate().translationY(-mToolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
//    }
//
//    private void showViews() {
//        mToolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
//    }

    class LoadTimelineItemsTask extends AsyncTask<Object, Void, List<TimelineItemDTO>> {

        @Override
        protected List<TimelineItemDTO> doInBackground(Object... params) {
            try {
                int count = (int)params[0];
                Date lastQueryTime = (Date)params[1];
                Date olderItem = (Date)params[2];
                DataSupplier client = RestClient.getInstance();
                return client.loadTimelineItems(count, lastQueryTime, olderItem);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(List<TimelineItemDTO> items) {
            timelineItemDTOs.addAll(items);
            timelineAdapter = new RecyclerTimelineAdapter(timelineItemDTOs, mContext);
            recyclerView.setAdapter(timelineAdapter);
            progressBar.setVisibility(View.GONE);
            timelineAdapter.setOnItemClickListener(onItemClickListener);
        }
    }
    class LoadMoreTimelineItemsTask extends AsyncTask<Object, Void, List<TimelineItemDTO>> {

        @Override
        protected List<TimelineItemDTO> doInBackground(Object... params) {
            try {
                int count = (int)params[0];
                Date lastQueryTime = (Date)params[1];
                Date olderItem = (Date)params[2];
                DataSupplier client = RestClient.getInstance();
                return client.loadTimelineItems(count, lastQueryTime, olderItem);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(List<TimelineItemDTO> items) {
            timelineItemDTOs.addAll(items);
            // For efficiency purposes, notify the adapter of only the elements that got changed
            // curSize will equal to the index of the first element inserted because the list is 0-indexed
            int curSize = timelineAdapter.getItemCount();
            timelineAdapter.notifyItemRangeInserted(curSize, items.size() - 1);
        }
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }
}
