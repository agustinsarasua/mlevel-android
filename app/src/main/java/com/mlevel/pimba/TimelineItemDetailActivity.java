package com.mlevel.pimba;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.adapter.RecyclerTimelineAdapter;
import com.mlevel.pimba.adapter.TransitionAdapter;
import com.mlevel.pimba.backend.DataSupplier;
import com.mlevel.pimba.backend.RestClient;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;

public class TimelineItemDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PARAM_ID = "item_id";

    //public Button mBtnEstoyPara;
    public ImageView mCover;

    public TextView mDescription;

    public TextView title;
    public TextView subtitle;

    public TextView txtDay;
    public TextView txtMonth;
    public TextView txtStartHour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDescription = (TextView) findViewById(R.id.itemDescription);
        mCover = (ImageView) findViewById(R.id.cover);
        txtDay = (TextView) findViewById(R.id.txtDay);
        txtMonth = (TextView) findViewById(R.id.txtMonth);
        //txtStartHour = (TextView) findViewById(R.id.txtStartHour);
        //mDescription = (TextView) findViewById(R.id.itemDescription);

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        String uuid = getIntent().getStringExtra(EXTRA_PARAM_ID);
        new LoadTimelineItemByIDTask().execute(uuid);

        windowTransition();
    }

    private void windowTransition() {
        getWindow().setEnterTransition(makeEnterTransition());
        getWindow().getEnterTransition().addListener(new TransitionAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                //btnEstoyPara.animate().alpha(1.0f);
                getWindow().getEnterTransition().removeListener(this);
            }
        });
    }

    public static Transition makeEnterTransition() {
        Transition fade = new Fade();
        //fade.excludeTarget(android.R.id.navigationBarBackground, true);
        //fade.excludeTarget(android.R.id.statusBarBackground, true);
        return fade;
    }

    class LoadTimelineItemByIDTask extends AsyncTask<String, Void, TimelineItemDTO> {

        @Override
        protected TimelineItemDTO doInBackground(String... params) {
            try {
                String uuid = params[0];
                DataSupplier client = RestClient.getInstance();
                return client.loadEventById(uuid);
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(TimelineItemDTO item) {
            //setTitle(item.name);
            getSupportActionBar().setTitle(item.name);
            mDescription.setText(item.description);
            //holder.txtStartHour.setText(DateTimeFormat.forPattern("HH:mm").print(startTime));
            DateTime startTime = new DateTime(item.startTime);
            txtDay.setText(DateTimeFormat.forPattern("dd").print(startTime));
            txtMonth.setText(DateTimeFormat.forPattern("MMM").print(startTime).toUpperCase());
            if(item instanceof EventDTO) {
                //Download image using picasso library
                Picasso.with(getApplicationContext()).load(((EventDTO)item).coverDTO.source)
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .into(mCover);
            }
        }
    }
}
