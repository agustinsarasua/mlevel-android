package com.mlevel.pimba.util;

import com.mlevel.api.timeline.EventTypeDTO;
import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.R;

/**
 * Created by agustin on 23/04/16.
 */
public class IconUtils {

    public static int getTimelineItemIcon(TimelineItemDTO itemDTO){

        if(itemDTO instanceof EventDTO){
            EventDTO eventDTO = (EventDTO) itemDTO;
            EventTypeDTO.EventType type = EventTypeDTO.EventType.valueOf(eventDTO.eventType);
            switch (type){
                case CINEMA_FILM:
                    return R.drawable.ic_wallet_travel;
                case CONCERT:
                    return R.drawable.ic_wallet_travel;
                case FOOTBALL_MATCH:
                    return R.drawable.ic_soccer;
                case PROMO_AEREA:
                    return R.drawable.ic_wallet_travel;
                default:
                    break;
            }
        } else {
            return 0;
        }
        return R.drawable.ic_soccer;
    }
}
