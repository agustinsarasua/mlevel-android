package com.mlevel.pimba.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mlevel.api.timeline.ActionDTO;
import com.mlevel.api.timeline.EventDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.pimba.R;
import com.mlevel.pimba.util.IconUtils;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;


public class RecyclerTimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TimelineItemDTO> mItemList;

    OnItemClickListener mItemClickListener;

    private Context mContext;

    public RecyclerTimelineAdapter(List<TimelineItemDTO> itemList, Context context) {
        mItemList = itemList;
        mContext = context;
    }

    @Override
    public RecyclerTimelineViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_timeline_item_a, null);

        RecyclerTimelineViewHolder viewHolder = new RecyclerTimelineViewHolder(view, mContext);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        RecyclerTimelineViewHolder holder = (RecyclerTimelineViewHolder) viewHolder;
        TimelineItemDTO item = mItemList.get(position);

        if(item instanceof EventDTO) {
            //Download image using picasso library
            Picasso.with(mContext).load(((EventDTO)item).coverDTO.source)
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.coverImage);
            holder.commonActions = ((EventDTO) item).commonActions;
        }
        holder.uuid = item.uuid;
        holder.title.setText(item.name);
        holder.subtitle.setText(item.subtitle);
        holder.description.setText(item.description);
        holder.icon.setImageResource(IconUtils.getTimelineItemIcon(item));

        DateTime startTime = new DateTime(item.startTime);
        holder.txtStartHour.setText(DateTimeFormat.forPattern("HH:mm").print(startTime));
        holder.txtDay.setText(DateTimeFormat.forPattern("dd").print(startTime));
        holder.txtMonth.setText(DateTimeFormat.forPattern("MMM").print(startTime).toUpperCase());
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, String uuid);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public class RecyclerTimelineViewHolder extends RecyclerView.ViewHolder {

        public ImageView icon;
        public TextView title;
        public TextView subtitle;
        public ImageView coverImage;
        public TextView description;
        public Button btnEstoyPara;
        public List<ActionDTO> commonActions;
        private Context mContext;
        public TextView txtDay;
        public TextView txtMonth;
        public TextView txtStartHour;
        public String uuid;

        public RecyclerTimelineViewHolder(View view, Context context) {
            super(view);
            this.mContext = context;
            this.icon = (ImageView) view.findViewById(R.id.icon);
            this.title = (TextView) view.findViewById(R.id.title);
            this.subtitle = (TextView) view.findViewById(R.id.subtitle);
            this.coverImage = (ImageView) view.findViewById(R.id.cover);
            this.description = (TextView) view.findViewById(R.id.description);
            this.btnEstoyPara = (Button) view.findViewById(R.id.btnEstoyPara);
            this.txtDay = (TextView) view.findViewById(R.id.txtDay);
            this.txtMonth = (TextView) view.findViewById(R.id.txtMonth);
            this.txtStartHour = (TextView) view.findViewById(R.id.txtStartHour);


            this.btnEstoyPara.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    final Dialog dialog = new Dialog(mContext, R.style.AppCompatAlertDialogStyle);
                    dialog.setContentView(R.layout.actions_dialog);
                    dialog.setTitle("Para que estas?");
                    ListView actionList = (ListView) dialog.findViewById(R.id.commonActionsList);
                    actionList.setAdapter(new CommonActionAdapter(mContext, commonActions));
                    EditText editText = (EditText) dialog.findViewById(R.id.customActionText);
                    editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            boolean handled = false;
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                handled = true;
                            }
                            return handled;
                        }
                    });
                    dialog.show();
                }
            });
            this.coverImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(itemView, uuid);
                }
                }
            });

        }

    }
}
