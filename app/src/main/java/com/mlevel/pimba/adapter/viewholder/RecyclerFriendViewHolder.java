package com.mlevel.pimba.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mlevel.pimba.R;
import com.pkmmte.view.CircularImageView;

/**
 * Created by agustin on 24/04/16.
 */
public class RecyclerFriendViewHolder extends RecyclerView.ViewHolder {

    public CircularImageView userImage;
    public TextView userName;
    public TextView userMessage;

    private Context mContext;

    public RecyclerFriendViewHolder(View view, Context context) {
        super(view);
        this.mContext = context;
        this.userImage = (CircularImageView) view.findViewById(R.id.userImage);
        this.userName = (TextView) view.findViewById(R.id.txtUserName);
        this.userMessage = (TextView) view.findViewById(R.id.txtUserMessage);
    }
}
