package com.mlevel.pimba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mlevel.api.CategoryDTO;
import com.mlevel.pimba.R;

/**
 * Created by agustin on 03/05/16.
 */
public class RecyclerPersonalizeAdapter extends RecyclerView.Adapter<RecyclerPersonalizeAdapter.CategoryViewHolder> {

    private CategoryDTO.Type[] mItemList;

    private Context mContext;

    public RecyclerPersonalizeAdapter(Context context, CategoryDTO.Type[] items) {
        this.mItemList = items;
        this.mContext = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, null);
        CategoryViewHolder rcv = new CategoryViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        holder.countryName.setText(mItemList[position].name());
        //holder.countryPhoto.setImageResource(itemList.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.mItemList.length;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView countryName;
        public ImageView countryPhoto;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            countryName = (TextView)itemView.findViewById(R.id.country_name);
            countryPhoto = (ImageView)itemView.findViewById(R.id.country_photo);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
}
