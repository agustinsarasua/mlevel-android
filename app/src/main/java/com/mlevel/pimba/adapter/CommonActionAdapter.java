package com.mlevel.pimba.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mlevel.api.timeline.ActionDTO;
import com.mlevel.pimba.R;

import java.util.List;

/**
 * Created by agustin on 24/04/16.
 */
public class CommonActionAdapter extends ArrayAdapter<ActionDTO> {

    public CommonActionAdapter(Context context, List<ActionDTO> actions) {
        super(context, 0, actions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ActionDTO user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.common_action_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.actionText);
        tvName.setText(user.name);
        // Return the completed view to render on screen
        return convertView;
    }
}
