package com.mlevel.pimba.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlevel.api.UserDTO;
import com.mlevel.pimba.R;
import com.mlevel.pimba.adapter.viewholder.RecyclerFriendViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by agustin on 24/04/16.
 */
public class RecyclerFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<UserDTO> mItemList;

    private Context mContext;

    public RecyclerFriendAdapter(List<UserDTO> itemList, Context context) {
        mItemList = itemList;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_user_item, null);

        RecyclerFriendViewHolder viewHolder = new RecyclerFriendViewHolder(view, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        RecyclerFriendViewHolder holder = (RecyclerFriendViewHolder) viewHolder;
        UserDTO item = mItemList.get(position);

        //Download image using picasso library
        Picasso.with(mContext).load(item.coverDTO.source)
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(holder.userImage);
        holder.userName.setText(item.name + " " + item.lastName);
        holder.userMessage.setText(item.message);
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }
}
