package com.mlevel.pimba.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mlevel.api.CategoryDTO;
import com.mlevel.pimba.R;

/**
 * Created by agustin on 26/04/16.
 */
public class PersonalizeAdapter extends BaseAdapter {

    private CategoryDTO.Type[] mItemList;

    private Context mContext;

    private static LayoutInflater inflater=null;

    public class Holder
    {
        TextView categoryName;
        ImageView categoryIcon;
    }

    public PersonalizeAdapter(Context context, CategoryDTO.Type [] items){
        this.mItemList = items;
        this.mContext = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mItemList.length;
    }

    @Override
    public Object getItem(int position) {
        return mItemList[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        CategoryDTO.Type type = mItemList[position];
        rowView = inflater.inflate(R.layout.category_item, null);
        holder.categoryName =(TextView) rowView.findViewById(R.id.country_name);
        holder.categoryIcon =(ImageView) rowView.findViewById(R.id.country_photo);

        //CategoryDTO.Type type = CategoryDTO.Type.valueOf(categoryDTO.name);
        switch (type){
            case ART:
                holder.categoryName.setText(R.string.category_art);
                break;
            case BUSINESS:
                holder.categoryName.setText(R.string.category_business);
                break;
            case CHARITY:
                holder.categoryName.setText(R.string.category_charity);
                break;
            case FASHION:
                holder.categoryName.setText(R.string.category_fashion);
                break;
            case FILMS:
                holder.categoryName.setText(R.string.category_films);
                break;
            case FOOD_AND_DRINK:
                holder.categoryName.setText(R.string.category_food_and_drink);
                break;
            case HEALTH:
                holder.categoryName.setText(R.string.category_health);
                break;
            case HOBBIES:
                holder.categoryName.setText(R.string.category_hobbies);
                break;
            case MUSIC:
                holder.categoryName.setText(R.string.category_music);
                break;
            case OTHER:
                holder.categoryName.setText(R.string.category_other);
                break;
            case SPORTS:
                holder.categoryName.setText(R.string.category_sports);
                break;
            case POLITICS:
                holder.categoryName.setText(R.string.category_politics);
                break;
            default:
                break;
        }
        return rowView;
    }
}
