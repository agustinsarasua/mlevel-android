package com.mlevel.pimba.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

/**
 * Created by agustin on 25/03/16.
 */
public class RestConnector {

    private String AUTH_HEADER = "Authorization";

    private String DEFAULT_HOST = "http://192.168.0.9:9290";

    private String CONTENT_TYPE = "application/json";

    private int DEFAULT_PORT = 9290;

    public static String AUTH_TOKEN = "";

    public Object doPost(String uri, Object body, Class clazz, String... params){
        //can catch a variety of wonderful things
        OutputStream os = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        Object result = null;
        try {
            //constants
            URL url = new URL(this.buildUrl(uri, params));
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
            String message = objectMapper.writeValueAsString(body);

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //open
            conn.connect();

            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            //clean up
            os.flush();

            is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = objectMapper.readValue(sb.toString(), clazz);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            //clean up
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }
    }

    public Object doGet(String uri, Class clazz, String... params){
        OutputStream os = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        Object result = null;
        try {
            //constants
            URL url = new URL(this.buildUrl(uri, params));
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //open
            conn.connect();

            is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = objectMapper.readValue(sb.toString(), clazz);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            //clean up
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }
    }

    public Object doPut(String uri, Object body, Class clazz, String... params){
        //can catch a variety of wonderful things
        OutputStream os = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        Object result = null;
        try {
            //constants
            URL url = new URL(this.buildUrl(uri, params));
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
            String message = objectMapper.writeValueAsString(body);

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("PUT");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //open
            conn.connect();

            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            //clean up
            os.flush();

            is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = objectMapper.readValue(sb.toString(), clazz);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            //clean up
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }
    }

    private String buildUrl(String serviceUrl, Object[] uriParams) {
        String urlSuffix = null;
        if(uriParams != null && uriParams.length > 0) {
            String partialUrl = serviceUrl.replaceAll("\\{\\}", "%s");
            urlSuffix = String.format(partialUrl, uriParams);
        } else {
            urlSuffix = serviceUrl;
        }
        return this.DEFAULT_HOST + urlSuffix;
    }
}
